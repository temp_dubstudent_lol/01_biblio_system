﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace _1_biblio_system
{
    class Program
    {
        static void Main(string[] args)
        {
            Book bk1 = new Book(10, "Толстой", "Война и мир");
            Book bk2 = new Book(11, "Достоевский", "Преступление и наказание");
            
            Console.WriteLine(bk1.ToString());
            Console.WriteLine(bk2.ToString());

            BookReader rdr1 = new BookReader("Аноним", 2002);
            rdr1.TakeBook(bk1);
            rdr1.TakeBook(bk2);

            PrintList(rdr1.GetList());

            rdr1.BookBack(bk2.id);
            PrintList(rdr1.GetList());
            rdr1.BookBack();
            PrintList(rdr1.GetList());

            string file_name = "readers.bin";
            BinaryFormatter bf = new BinaryFormatter();
        }

        static void PrintList(List<Book> list)
        {
            Console.WriteLine("List:");
            foreach(Book b in list)
            {
                Console.WriteLine(b);
            }
        }
    }
}
